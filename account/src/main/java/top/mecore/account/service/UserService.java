package top.mecore.account.service;

import top.mecore.account.entity.User;

import java.util.List;

public interface UserService {
    List<User> listAllUsers();
}
