package top.mecore.account.service.impl;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import top.mecore.account.entity.User;
import top.mecore.account.repository.UserRepository;
import top.mecore.account.service.UserService;

import java.util.List;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    @Override
    public List<User> listAllUsers() {
        return userRepository.findAll();
    }
}
