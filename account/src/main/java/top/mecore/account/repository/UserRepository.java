package top.mecore.account.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import top.mecore.account.entity.User;

public interface UserRepository extends JpaRepository<User, Long> {
}
